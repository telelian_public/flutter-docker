FROM ubuntu:18.04

# RUN apt-get update && \
#     apt-get install -y bash curl file git unzip xz-utils zip libglu1-mesa \
#     cmake libgtk-3-dev ninja-build liblzma-dev clang build-essential pkg-config libegl1-mesa-dev libxkbcommon-dev libgles2-mesa-dev \
#     && apt-get autoremove -y && apt-get clean \
#     && rm -rf /var/lib/apt/lists/*


RUN apt-get update && \
    apt-get install -y bash curl file git unzip xz-utils zip \
    clang cmake build-essential pkg-config software-properties-common \
    libegl1-mesa-dev libxkbcommon-dev libgles2-mesa-dev \
    ninja-build libgtk-3-dev \
    && add-apt-repository ppa:git-core/ppa -y \
    && apt-get update \
    && apt-get install git -y \
    && apt-get autoremove -y && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt/

# ARG flutterVersion=stable
# ADD https://api.github.com/repos/flutter/flutter/compare/${flutterVersion}...${flutterVersion} /dev/null

# ARG flutterVersion=3.8.0-0.0.pre
# ARG flutterVersion=3.3.10
# RUN git clone https://github.com/flutter/flutter.git -b ${flutterVersion} flutter-sdk

RUN git clone https://github.com/sony/flutter-elinux.git

RUN flutter-elinux/bin/flutter-elinux precache
RUN flutter-elinux/bin/flutter-elinux config --no-analytics

ENV PATH="$PATH:/opt/flutter-elinux/bin"
ENV PATH="$PATH:/opt/flutter-elinux/bin/cache/dart-sdk/bin"

RUN flutter-elinux doctor