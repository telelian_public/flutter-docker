# flutter-docker

## build
```bash
docker build -t registry.gitlab.com/telelian_public/flutter-docker .
docker push registry.gitlab.com/telelian_public/flutter-docker
```

## run
```bash
export DISPLAY=:0
xhost +local:docker

docker run -it --privileged --rm \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-v $HOME/work:/root/work \
-e DISPLAY=unix:0 \
registry.gitlab.com/telelian_public/flutter-docker /bin/bash
```

## test
```
flutter-elinux create sample && cd sample && flutter-elinux run
```